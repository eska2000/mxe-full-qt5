##
## Docker image for compiling static QT Windows desktop applications
##

FROM debian:stretch-slim

RUN QT_INSTALLATION_DEPS='autoconf \
    automake \
    autopoint \
    bash \
    bison \
    bzip2 \
    flex \
    g++ \
    g++-multilib \
    gettext \
    git \
    gperf \
    intltool \
    libc6-dev-i386 \
    libgdk-pixbuf2.0-dev \
    libltdl-dev \
    libssl-dev \
    libtool-bin \
    libxml-parser-perl \
    lzip \
    make \
    openssl \
    p7zip-full \
    patch \
    perl \
    pkg-config \
    python \
    ruby \
    sed \
    unzip \
    wget \
    xz-utils' \
    && apt-get update \
    && apt-get -qq install --no-install-recommends $QT_INSTALLATION_DEPS \
\
    && git clone https://github.com/mxe/mxe.git \
    && CORES="$(grep -c ^processor /proc/cpuinfo)" \
    && cd mxe && make -j${CORES} qt5 \
    && export PATH=/mxe/usr/bin:$PATH